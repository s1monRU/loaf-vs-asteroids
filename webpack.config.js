let path = require('path');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
 
module.exports = {
  entry: {
      'bundle.js': './assets/js/index.js',
      'bundle.css': './assets/css/index.js'
  },
  output: {
    filename: '[name]',
    path: path.resolve(__dirname, 'dist')
  },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader'
                })
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {},
                    },
                ],
            },
        ],
    },
    resolve: {
        alias: {
            handlebars: 'handlebars/dist/handlebars.min.js'
        }
    },
    plugins: [
        new ExtractTextPlugin("bundle.css"),
    ]
};