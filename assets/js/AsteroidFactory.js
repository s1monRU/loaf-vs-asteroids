import Asteroid from './Asteroid';

/**
 * Factory class for creating asteroids
 */
class AsteroidFactory {

    /**
     * Create a factory
     * @param loaf {Loaf} The loaf
     * @param difficulty {string} Game difficulty
     */
    constructor(loaf, difficulty) {
        this.loaf = loaf;
        this.difficulty = difficulty;
        this.interval = null;
    }

    /**
     * Initialize the factory.
     * Run asteroids with a speed that depends on game difficulty
     */
    init() {
        let interval = 1000;
        switch (this.difficulty) {
            case 'easy':
                interval = 2000; break;
            case 'middle':
                interval = 1000; break;
            case 'hard':
                interval = 700; break;
            case 'hardcore':
                interval = 300; break;
        }
        this.interval = setInterval(this.create, interval, this.loaf);
    };

    /**
     * Create an asteroid
     * @param loaf {Loaf} The loaf
     */
    create(loaf) {
        let asteroid = new Asteroid(loaf);
        asteroid.create();
    };

    /**
     * Stop the factory
     */
    stop() {
        clearInterval(this.interval);
    }
}

export default AsteroidFactory;