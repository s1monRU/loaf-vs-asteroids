import Health from './Health.js';
import TheEnd from './TheEnd.js';
import ScoreSaver from "./backendCommunicators/ScoreSaver";
import Points from "./Points";
import Logger from "./backendCommunicators/Logger";

/**
 * Loaf class
 */
class Loaf {

    /**
     * Initialize the loaf
     */
    constructor() {
        this.element = document.createElement('div');
        this.element.id = 'buh';
        this.element.style.left = '0px';
        this.element.style.top = '0px';

        this.health = 100;

        this.leftPressed = false;
        this.rightPressed = false;
        this.upPressed = false;
        this.downPressed = false;

        setInterval(Loaf.move, 25, this);
    }

    /**
     * Crash the loaf.
     * Method reduces the loaf's health, destroyes the loaf is health is less than zero, finishes the game
     */
    crash() {
        new Logger('loaf crashed', {loaf: this});
        this.health -= Math.round(Math.random() * (25 - 5) + 5);

        if(this.health <= 0) {
            new Logger('loaf destroyed', {loaf: this});
            this.health = 0;

            ScoreSaver.save(Points.earned);

            let theEnd = new TheEnd();
            theEnd.show();

            // Stops AsteroidFactory
            this.destroy();

            this.element.remove();
            delete this.element;
        }

        let healthBar = new Health(this.health);
        healthBar.show();
    };

    /**
     * Invoke a destroy event when the loaf is totally crashed
     */
    destroy() {
        let destroyEvent = new CustomEvent('destroy', {
            bubbles: true,
            detail: ''
        });
        this.element.dispatchEvent(destroyEvent);
    };

    /**
     * Restrict the loaf to move outside of a game field
     * @param self Current instance of the loaf
     * @returns {boolean} If the loaf wents off the screen
     */
    static isWentOffTheScreen(self) {
        if(self.leftPressed && self.element.offsetLeft <= self.element.parentElement.offsetLeft) return true;
        if(self.upPressed && self.element.offsetTop <= self.element.parentElement.offsetTop) return true;

        if(self.rightPressed &&
            self.element.offsetLeft >=
            self.element.parentElement.clientWidth - self.element.clientWidth +
            self.element.parentElement.offsetLeft
        ) return true;

        if(self.downPressed &&
            self.element.offsetTop >=
            self.element.parentElement.clientHeight - self.element.clientHeight
        ) return true;

        return false;
    }

    /**
     * Move the loaf in the direction that depends on which key is pressed
     * @param self Current instance of the loaf
     */
    static move(self) {
        if(!Loaf.isWentOffTheScreen(self)) {
            if(self.leftPressed) {
                self.element.style['left'] = parseInt(self.element.style['left'])-3 + 'px';
            }
            if(self.upPressed) {
                self.element.style['top'] = parseInt(self.element.style['top'])-3 + 'px';
            }
            if(self.rightPressed) {
                self.element.style['left'] = parseInt(self.element.style['left'])+3 + 'px';
            }
            if(self.downPressed){
                self.element.style['top'] = parseInt(self.element.style['top'])+3 + 'px';
            }
        }
    };

    /**
     * Start to move the loaf
     * @param e {KeyboardEvent} Keyboard event
     */
    startMoving(e) {
        switch (e.which) {
            case 37: // left
                this.leftPressed = true;
                break;
            case 38: // up
                this.upPressed = true;
                break;
            case 39: //right
                this.rightPressed = true;
                break;
            case 40: // down
                this.downPressed = true;
                break;
        }
    };

    /**
     * Stop the loaf
     * @param e {KeyboardEvent} Keyboard event
     */
    stopMoving(e) {
        switch (e.which) {
            case 37: // left
                this.leftPressed = false;
                break;
            case 38: // up
                this.upPressed = false;
                break;
            case 39: // right
                this.rightPressed = false;
                break;
            case 40: // down
                this.downPressed = false;
                break;
        }
    };
}

export default Loaf;