import Bullet from "./Bullet";
import Logger from "./backendCommunicators/Logger";

/**
 * Factory class for creating bullets
 */
class BulletFactory {

    /**
     * Create a bullet
     * @param loaf {Loaf} The loaf
     * @param e {event} Keyboard event
     */
    constructor(loaf, e) {
        if(e.which === 32 || e.which === 17) {
            let bullet = new Bullet(loaf).create();
            new Logger('shot', {loaf: loaf, bullet: bullet});
        }
    }

    /**
     * Restrict to run the bullet
     * @param time {int} Time gap while bullets running is restricted
     */
    static restrict(time) {
        time = time || 300;
        this.allow = false;

        setTimeout(() => {
            this.allow = true;
        }, time)
    }

    /**
     * Check if it is allowed to run the bullet
     * @returns {boolean} Bullet running is or not allowed now
     */
    static isAllowed() {
        if(BulletFactory.allow === false) {
            return false;
        }

        this.restrict();
        return true;
    }

}

/**
 * Bullet running is allowed by default
 * @type {boolean}
 */
BulletFactory.allow = true;

export default BulletFactory;