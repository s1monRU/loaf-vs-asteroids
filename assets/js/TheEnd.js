import axios from 'axios';
import Handlebars from 'handlebars';
import Points from "./Points";
import Logger from "./backendCommunicators/Logger";

/**
 * The ending screen
 */
class TheEnd {

    /**
     * Initialize the class
     */
    constructor() {
        new Logger('finished');
        TheEnd.period = 'month';
        this.element = document.getElementById('the-end');
    }

    /**
     * Render the finish screen
     */
    show() {
        axios.get('templates/the-end.html')
            .then((theEndPage) => {
                this.element.style.display = 'block';
                this.element.innerHTML = theEndPage.data;
                TheEnd.source = document.getElementById('leaderboard-template').innerHTML;
                Points.showInTheEnd();
                TheEnd.showLeaderboard(TheEnd.period);
            })
    }

    /**
     * Render the leaderboard
     * @param period {string} Period of time for the leaderboard
     */
    static showLeaderboard(period) {
        TheEnd.period = period;
        axios.get('/api/show-leaderboard/'+TheEnd.period)
            .then((leaderboardJSON) => {
                let context = {
                    leaderboard: leaderboardJSON.data
                };
                let template = Handlebars.compile(TheEnd.source);
                let html = template(context);
                let leaderboardTable = document.getElementById('leaderboard-tbody');
                leaderboardTable.innerHTML = html;
            });
    }

    /**
     * Store the player's name
     */
    static sendPlayerName() {
        let form = document.getElementById('save-name');
        let errorBlock = document.getElementById('errorSavingNameBlock');
        let formData = new FormData(form);
        let name = formData.get('player-name');

        axios.post('/api/save-player-name', {
            token: sessionStorage.getItem('token'),
            name: name
        }).then(() => {
            let successBlock = document.getElementById('successSavingNameBlock');
            let fieldset = document.getElementById('save-name-fieldset');

            errorBlock.style.display = 'none';
            successBlock.style.display = 'block';
            fieldset.setAttribute('disabled', null);

            TheEnd.showLeaderboard(TheEnd.period);
        }).catch(() => {
            errorBlock.style.display = 'block';
        })
    }

}

export default TheEnd;