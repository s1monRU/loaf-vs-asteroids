import axios from 'axios';
import Points from "../Points";

/**
 * Game events logger
 */
class Logger {

    /**
     * Save a game event
     * @param event {string} Type of an event
     * @param properties {object} Specialities of an event
     */
    constructor(event, properties) {
        properties = properties || {};

        axios.post('/api/log', {
            token: sessionStorage.getItem('token'),
            score: Points.earned,
            event: event,
            properties: properties
        }).catch(() => {
            console.error('Cannot log it');
        })
    }

}

export default Logger;