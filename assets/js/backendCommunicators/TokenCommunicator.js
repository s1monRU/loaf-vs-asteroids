import axios from 'axios';

/**
 * Provider that gets and stores a token for a game
 */
class TokenCommunicator {

    /**
     * Get a token
     */
    static getToken() {
        return axios.get('/api')
            .then((token) => {
                this.storeToken(token);
            })
            .catch(() => {
                console.error('Could not get token');
            })
    }

    /**
     * Save a token into session storage for the current game
     * @param token {string} Game token
     */
    static storeToken(token) {
        sessionStorage.setItem('token', token.data);
    }

}

export default TokenCommunicator;