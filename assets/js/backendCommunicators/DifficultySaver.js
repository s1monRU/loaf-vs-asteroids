import axios from 'axios';

/**
 * Saver for game difficulty
 */
class DifficultySaver {

    /**
     * Save difficulty
     * @param difficulty {string} Game difficulty
     */
    static save(difficulty) {
        axios.post('/api/save-difficulty', {
            token: sessionStorage.getItem('token'),
            difficulty: difficulty
        }).catch(() => {
            console.error('Difficulty has not been saved');
        })
    }
}

export default DifficultySaver;