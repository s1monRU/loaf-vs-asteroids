import axios from 'axios';

/**
 * Saver for game score
 */
class ScoreSaver {

    /**
     * Save score
     * @param score {int} Game score
     */
    static save(score) {
        axios.post('/api/save-score', {
            token: sessionStorage.getItem('token'),
            score: score
        }).catch(() => {
            console.error('Score has not been saved');
        })
    }
}

export default ScoreSaver;