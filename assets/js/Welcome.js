import axios from 'axios';

/**
 * Welcome screen
 */
class Welcome {

    /**
     * Get a welcome element
     */
    constructor() {
        this.element = document.getElementById('welcome');
    }

    /**
     * Render the screen
     */
    show() {
        axios.get('templates/welcome.html')
            .then((welcomePage) => {
                this.element.innerHTML = welcomePage.data;
            })
    };

    /**
     * Remove the screen
     */
    remove() {
        this.element.remove();
        delete this.element;
    }

}

export default Welcome;