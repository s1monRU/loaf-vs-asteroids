import {mainPlace} from './MainPlace.js';
import Points from "./Points";

/**
 * Asteroid class
 */
class Asteroid {

    /**
     * Initialize an asteroid
     * @param buhank The loaf
     */
    constructor(buhank) {
        this.buhank = buhank;

        this.element = document.createElement('div');
        this.element.className = 'asteroid';
        this.element.style.width = '6em';
        this.element.style.height = '3em';
        this.speed = 75;
    }

    /**
     * Create and render an asteroid
     */
    create() {
        let availableHeight = {
            min: parseInt(mainPlace.offsetTop),
            max: parseInt(mainPlace.offsetTop + mainPlace.clientHeight - parseInt(this.element.style.height)*16),
        };

        this.element.style.left = mainPlace.offsetLeft + mainPlace.clientWidth - parseInt(this.element.style.width)*16 + 'px';
        this.element.style.top = Math.random() * (availableHeight.max - availableHeight.min) + availableHeight.min + 'px';
        mainPlace.appendChild(this.element);

        this.offsetLeft = parseInt(this.element.style.left);

        this.run();
    };

    /**
     * Run the asteroid with specified speed
     */
    run() {
        let flight = setInterval((function(self){
            return function(){
                self.offsetLeft-=4;
                self.element.style.left = self.offsetLeft + 'px';
                if (parseInt(self.element.style.left) <= mainPlace.offsetLeft) {
                    clearInterval(flight);

                    Points.add(2);
                    Points.show();

                    self.element.remove();
                    delete self.element;
                }

                /**
                 * Handle a collision between an asteroid and the loaf
                 */
                let loaf = self.buhank.element;
                if(
                    (self.offsetLeft <= parseInt(loaf.offsetLeft + loaf.clientWidth)) &&
                    (loaf.offsetLeft < self.offsetLeft)
                ) {
                    if(
                        (parseInt(loaf.offsetTop + loaf.clientHeight) >= self.element.offsetTop) &&
                        (loaf.offsetTop <= self.element.offsetTop + self.element.clientHeight)
                    ) {
                        clearInterval(flight);

                        self.element.remove();
                        delete self.element;

                        self.buhank.crash();
                    }
                }
            }
        })(this), 1000/this.speed*2)
    };
}

export default Asteroid;