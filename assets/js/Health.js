import Chartist from 'chartist';
import chart from 'chartist-plugin-fill-donut';

/**
 * The loaf's health
 */
class Health {

    /**
     * Set the loaf's health
     * @param current {int} The loaf's health
     */
    constructor(current) {
        this.current = current || 0;
    }

    /**
     * Render a pie chart that contains current health level
     */
    show() {
        new Chartist.Pie('#health',
            {
                series: [this.current],
                labels: ['', '']
            }, {
                donut: true,
                donutWidth: 20,
                startAngle: 210,
                total: 120,
                showLabel: false,
                plugins: [
                    Chartist.plugins.fillDonut({
                        items: [{
                            content: '&hearts;',
                            position: 'bottom',
                            offsetY : 10,
                            offsetX: -2
                        }, {
                            content: '<h3>'+this.current+'<span class="small">&nbsp;HP</span></h3>'
                        }]
                    })
                ],
            });
    }
}

export default Health;