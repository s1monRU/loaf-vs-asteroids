import {mainPlace} from './MainPlace.js';
import BulletFactory from './BulletFactory.js';
import Points from './Points.js';
import Logger from "./backendCommunicators/Logger";

/**
 * Bullet class
 */
class Bullet {

    /**
     * Initialize a bullet
     * @param loaf {Loaf} The loaf
     */
    constructor(loaf) {
        this.loaf = loaf;

        this.element = document.createElement('div');
        this.element.className = 'bullet';
        this.element.style.top;
        this.element.style.left;

        this.speed = 1000;
    }

    /**
     * Create and render a bullet
     * @returns {*} Current instance of a bullet
     */
    create() {
        if(!BulletFactory.isAllowed()) {
            return;
        }

        this.element.style.top = parseInt(this.loaf.element.style.top) + this.loaf.element.clientHeight/2 + 'px';
        this.element.style.left = this.loaf.element.parentElement.offsetLeft + parseInt(this.loaf.element.style.left) + this.loaf.element.clientWidth + 'px';

        this.offsetTop = parseInt(this.element.style.top);
        this.offsetLeft = parseInt(this.element.style.left);
        this.height = this.element.clientHeight;

        mainPlace.appendChild(this.element);
        this.run();
        return this;
    };

    /**
     * Run the bullet with specified speed
     */
    run() {
        let flight = setInterval((function(self){
            return function(){
                self.offsetLeft++;
                self.element.style.left = self.offsetLeft + 'px';
                if (self.offsetLeft >= mainPlace.offsetLeft + mainPlace.clientWidth - self.element.clientWidth) {
                    clearInterval(flight);
                    self.element.remove();
                    delete self.element;
                }

                /**
                 * Handle a collision between a bullet and an asteroid
                 */
                let asteroids = document.getElementsByClassName('asteroid');
                for(let i = 0; i < asteroids.length; i++){
                    let asteroidOffsetLeft = parseInt(asteroids[i].style.left);
                    let asteroidOffsetTop = parseInt(asteroids[i].style.top);
                    let asteroidHeight = asteroids[i].clientHeight;
                    if(
                        parseInt(self.offsetLeft+self.element.clientWidth) >= (asteroidOffsetLeft) &&
                        asteroidOffsetLeft > parseInt(self.offsetLeft)
                    ){
                        if(
                            (asteroidOffsetTop + asteroidHeight >= self.offsetTop) &&
                            (asteroidOffsetTop + asteroidHeight <= self.offsetTop + self.height + asteroidHeight)
                        ){
                            new Logger('asteroid crashed', {bullet: self});

                            clearInterval(flight);

                            Points.add(5);
                            Points.show();

                            self.element.remove();
                            delete self.element;

                            // This isn't working:( It should be taken from Asteroid.element or from factory
                            document.getElementsByClassName('asteroid')[i].remove();
                            delete document.getElementsByClassName('asteroid')[i];
                        }
                    }

                }
            }
        })(this), 1000/this.speed)
    }
}

export default Bullet;