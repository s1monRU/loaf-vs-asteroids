/**
 * Points class. Works with the game score
 */
class Points {

    /**
     * Add some points to the score
     * @param howMany {int} How many points to add
     */
    static add(howMany) {
        Points.earned+=howMany;
    }

    /**
     * Render the amount of points
     * @returns {boolean}
     */
    static show() {
        let pointsValueElement = document.getElementById('points-value');
        pointsValueElement.innerText = Points.earned;
        return true;
    }

    /**
     * Render the amount of points on the finish screen
     * @returns {boolean}
     */
    static showInTheEnd() {
        let theEndScreenPoints = document.getElementById('points-value-the-end');
        if(theEndScreenPoints) {
            theEndScreenPoints.innerText = Points.earned;
        }
        return true;
    }

}

/**
 * No points by default
 * @type {int}
 */
Points.earned = 0;

export default Points;