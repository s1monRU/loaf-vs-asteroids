import Health from "./Health.js";
import Loaf from "./Loaf.js";
import Welcome from "./Welcome.js";
import Bullet from "./Bullet.js";
import AsteroidFactory from "./AsteroidFactory.js";
import BulletFactory from "./BulletFactory";
import TokenCommunicator from "./backendCommunicators/TokenCommunicator";
import DifficultySaver from "./backendCommunicators/DifficultySaver";
import TheEnd from "./TheEnd";
import Logger from "./backendCommunicators/Logger";
import Points from "./Points";


/**
 * The main Game class
 * @author Ivan Semenov <developer.ivan.semenov@gmail.com>
 */
class Game {

    /**
     * Create a game
     */
    constructor() {
        this.difficulty = 'middle';
        this.buhank = new Loaf();
        this.welcome = new Welcome();
    }

    /**
     * Initialize a new game. Set game environment and init the actions
     */
    init() {
        console.log('Game has been initialized');
        sessionStorage.clear();
        TokenCommunicator.getToken().then(() => new Logger('init'));

        this.welcome.show();

        let health = new Health(100);
        health.show();

        this.mainPlace = document.getElementById('main-place');
        this.mainPlace.appendChild(this.buhank.element);

        this.initActions();
    };

    /**
     * Initialize user actions such as keypresses and clicks.
     */
    initActions() {
        let buhank = this.buhank;
        let self = this;

        /**
         * Move the loaf
         * @param e Keyboard event
         */
        window.onkeydown = function(e) {
            buhank.startMoving(e);
            new BulletFactory(buhank, e);
        };

        /**
         * Stop moving the loaf
         * @param e Keyboard event
         */
        window.onkeyup = function(e) {
            buhank.stopMoving(e);
        };

        /**
         * Start the game or set difficulty or something else that depends on which button is pressed
         * @param e Mouse event
         */
        window.onclick = function(e) {
            let target = e.target || e.srcElement;

            if(target.id === 'start-game') {
                new Logger('started', {loaf: buhank});
                this.welcome.remove();

                DifficultySaver.save(self.difficulty);
                self.asteroidFactory = new AsteroidFactory(buhank, self.difficulty);
                self.asteroidFactory.init();
            }

            if(target.dataset.difficulty){
                document.querySelector('.active').classList.remove('active');
                target.classList.add('active');
                self.difficulty = target.dataset.difficulty;
            }

            if(target.dataset.period){
                document.querySelector('.active').classList.remove('active');
                target.classList.add('active');
                TheEnd.showLeaderboard(target.dataset.period);
            }
        };

        /**
         * Submit form. Save player's name
         * @param e Form event
         */
        window.onsubmit = function(e) {
            e.preventDefault();
            if(e.target.id === 'save-name') {
                TheEnd.sendPlayerName();
            }

        };

        /**
         * Stop creating asteroids when the loaf is destroyed
         * @param {callback} event - The callback that stops AsteroidFactory
         */
        buhank.element.addEventListener('destroy', function(event) {
            self.asteroidFactory.stop();
        });
    };

}

export default Game;